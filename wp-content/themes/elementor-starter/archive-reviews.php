<?php
/**
 * The template for displaying archive pages.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}
?>
<?php get_header(); ?>
<div class="container-fluid bg-linear">
    <?php echo do_shortcode( '[elementor-template id="429"]' ); ?>
    <div class="related-block">
    <div class="row">
        <?php
        $args = array(
          'post_type'      => 'reviews',
          'posts_per_page' => -1,
          'order'          => 'DESC',
        );

        $all_posts = new WP_Query( $args );
        
        ?>
        <?php if ( $all_posts->have_posts() ) : // make sure we have posts to show before doing anything?>
        <?php while ( $all_posts->have_posts() ) : $all_posts->the_post(); ?>

        <div class="col-md-4">
            <div class="col-12">
                <div class="review">
                    <div class="review__info">
                        <h3><?= get_the_title(); ?></h3>
                        <p class="review__text"> <?= get_field('review'); ?></p>
                        <p class="review__namedate"> <?= get_field('name_date'); ?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
    </div>
        </div>
</div>

<?php get_footer();
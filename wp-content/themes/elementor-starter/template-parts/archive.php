<?php
/**
 * The template for displaying archive pages.
 *
 * @package HelloElementor
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}
?>
<?php echo do_shortcode('[elementor-template id="285"]'); ?>
<div class="container-fluid bg-linear">
    <div class="articles-block">
        <div class="row">
            <?php
          while ( have_posts() ) {
            the_post();
            $post_link = get_permalink();
            ?>
            <div class="col-md-4">
                <div class="col-12">
                    <div class="article">
                        <?php if ( has_post_thumbnail() ) : ?>
                        <img class="article__image"
                            src="<?= get_the_post_thumbnail_url( get_the_ID(), 'medium_large' ) ?>" alt="Image">
                        <?php endif; ?>
                        <div class="article__info">
                            <h3><?= get_the_title(); ?></h3>
                            <?php the_excerpt(); ?>
                            <a href="<?php echo get_permalink(); ?>" class="btn article__btn">Read More</a>
                        </div>
                    </div>
                </div>
            </div>


            <?php } ?>

        </div>
    </div>
</div>
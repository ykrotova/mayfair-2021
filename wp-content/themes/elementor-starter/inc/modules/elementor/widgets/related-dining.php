<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class Related_Dining extends Widget_Base {
  public function get_name() {
    return 'Related_Dining';
  }

  public function get_title() {
    return 'Related Dining Experience';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here

    $this->end_controls_section();
  }

  protected function render() {
    $settings = $this->get_settings_for_display();

    $uid = uniqid( 'sample-' );
    ?>
    <div class="you-may-also-like" id="<?= $uid ?>">
      <div class="container-fluid">  
      <div class="related-block">
          <div class="row">
          <?php
          $args = array(
            'post_type'      => 'dining-bars',
            'posts_per_page' => 3,
            'order'          => 'DESC',
            'post__not_in' => array( get_the_ID() )
          );

          $all_posts = new WP_Query( $args );
          ?>
           <?php if ( $all_posts->have_posts() ) : // make sure we have posts to show before doing anything?>
            <?php while ( $all_posts->have_posts() ) : $all_posts->the_post(); ?>

            <div class="col-md-4">
                <div class="col-12">
                    <div class="article">
                        <?php if ( has_post_thumbnail() ) : ?>
                        <img class="article__image"
                            src="<?= get_the_post_thumbnail_url( get_the_ID(), 'medium_large' ) ?>" alt="Image">
                          <?php else: ?>
                            <img class="article__image" src="/wp-content/uploads/2021/09/superior-queen.png" alt="Image">
                        <?php endif; ?>
                        <div class="article__info">
                            <h3><?= get_the_title(); ?></h3>
                            <div class="article_twolinks"><a href="<?php echo get_permalink(); ?>" class="btn article__btn">Find Out More</a>
                            <a href="<?php echo get_permalink(); ?>" class="btn article__btn">Book Now</a></div>
                        </div>
                    </div>
                </div>
            </div>




            <?php endwhile; ?>
            <?php endif; ?>
            </div>
                          </div>
        </div>
    </div>
    <?php
  }
}

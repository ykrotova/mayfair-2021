<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class Whats_On extends Widget_Base {
  public function get_name() {
    return 'Whats_On';
  }

  public function get_title() {
    return 'Whats On';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here

    $this->end_controls_section();
  }

  protected function render() {
    $settings = $this->get_settings_for_display();

    $uid = uniqid( 'sample-' );
    ?>
    <div class="you-may-also-like" id="<?= $uid ?>">
      <div class="container-fluid">  
      <div class="">
          <div class="row">
<div class="swiper-posts overflow-hidden">
  <!-- Additional required wrapper -->
  <div class="swiper-wrapper">
        <?php
          $args = array(
            'post_type'      => 'post',
            'posts_per_page' => 3,
            'order'          => 'DESC',
            'post__not_in' => array( get_the_ID() )
          );

          $all_posts = new WP_Query( $args );
          ?>
           <?php if ( $all_posts->have_posts() ) : // make sure we have posts to show before doing anything?>
            <?php while ( $all_posts->have_posts() ) : $all_posts->the_post(); ?>
            <div class="swiper-slide">
              <div class="col-md-12">
                <div class="article">
                        <?php if ( has_post_thumbnail() ) : ?>
                        <img class="article__image"
                            src="<?= get_the_post_thumbnail_url( get_the_ID(), 'medium_large' ) ?>" alt="Image">
                        <?php endif; ?>
                        <div class="article__info">
                            <h3><?= get_the_title(); ?></h3>
                            <?php the_excerpt(); ?>
                            <a href="<?php echo get_permalink(); ?>" class="btn article__btn">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
                         
          <?php endwhile; ?>
          <?php endif; ?>
             <!-- If we need navigation buttons -->
    </div>
    <div class="swiper-button-prev"></div>
  <div class="swiper-button-next"></div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <?php
  }
}

<?php

defined( 'ABSPATH' ) or exit;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class Swiper_Images_Text extends Widget_Base {
  public function get_name() {
    return 'Swiper_Images_Text';
  }

  public function get_title() {
    return 'Swiper For Images and Text';
  }

  public function get_icon() {
    return 'fa fa-puzzle-piece';
  }

  public function get_categories() {
    return [ 'custom' ];
  }

  protected function _register_controls() {
    $this->start_controls_section(
      'content_section',
      [
        'label' => 'Content',
        'tab'   => Controls_Manager::TAB_CONTENT,
      ]
    );

    // Add controls here
    $repeater = new \Elementor\Repeater();

    $repeater->add_control(
      'slider_image',
      [
        'label'   => __( 'Choose Image', 'plugin-domain' ),
        'type'    => \Elementor\Controls_Manager::MEDIA,
        'default' => [
          'url' => \Elementor\Utils::get_placeholder_image_src(),
        ],
      ]
    );

    $this->add_control(
      'slider',
      [
        'label'  => __( 'Slider Item', 'plugin-domain' ),
        'type'   => \Elementor\Controls_Manager::REPEATER,
        'fields' => $repeater->get_controls(),
      ]
    );

    $this->end_controls_section();
  }

  protected function render() {
    $settings = $this->get_settings_for_display();

    $uid = uniqid( 'sample-' );
    ?>
    <!-- Slider main container -->

    <!-- Slides -->

    <?php
  }
}

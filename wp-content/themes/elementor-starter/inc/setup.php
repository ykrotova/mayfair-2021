<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 */

defined( 'WP_ENV' ) or define( 'WP_ENV', 'staging' );
defined( 'ASSETS_VERSION' ) or define( 'ASSETS_VERSION', '1.0' );

// add_filter( 'acf/settings/show_admin', ( defined( 'SHOW_ACF' ) && SHOW_ACF ? '__return_true' : '__return_false' ) );

add_filter( 'body_class', 'body_classes' );
function body_classes( $classes ) {
  if ( ! is_singular() ) {
    $classes[] = 'hfeed';
  }

  return $classes;
}

add_action( 'wp_head', 'pingback_header' );
function pingback_header() {
  if ( is_singular() && pings_open() ) {
    echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
  }
}

add_action( 'after_setup_theme', 'setup_theme' );
function setup_theme() {
  add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption', ) );
  add_theme_support( 'automatic-feed-links' );
  add_theme_support( 'customize-selective-refresh-widgets' );

  add_theme_support( 'title-tag' );
  add_theme_support( 'post-thumbnails' );

  // add_image_size( 'thumbnails_600_516', 600, 516, true );

  add_post_type_support( 'post', 'excerpt' );

  register_nav_menus( array(
    'primary_menu' => __( 'Primary Menu' ),
    'footer_menu'  => __( 'Footer Menu' ),
  ) );
}

add_action( 'acf/init', 'add_site_settings_page' );
function add_site_settings_page() {
  if ( ( current_user_can( 'manage_options' ) || current_user_can( 'editor' ) ) && function_exists( 'acf_add_options_page' ) ) {
    acf_add_options_page( array(
      'page_title'    => 'Site Settings',
      'menu_title'    => 'Site Settings',
      'menu_slug'     => 'site-settings',
      'capability'    => 'edit_posts',
      'icon_url'      => 'dashicons-art',
      'update_button' => 'Save Settings',
      'redirect'      => false,
    ) );
  }
}

add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );
function enqueue_scripts() {
  $suffix = SCRIPT_DEBUG ? '' : '.min';

  /* Common assets */
  $assets_css = [
    'css-bootstrap-reboot' => 'plugins/bootstrap-4.5.0/css/bootstrap-reboot.min.css',
    'css-bootstrap-grid'   => 'plugins/bootstrap-4.5.0/css/bootstrap-grid.min.css',
    'css-bootstrap'        => 'plugins/bootstrap-4.5.0/css/bootstrap.min.css',
    'css-font-awesome'     => 'plugins/font-awesome-4.7.0/css/font-awesome.min.css',
  ];
  $assets_js  = [
    'js-bootstrap'   => 'plugins/bootstrap-4.5.0/js/bootstrap.bundle.min.js',
    'js-css-browser' => 'plugins/css_browser_selector/css_browser_selector.js',
  ];

  /* Specific assets */
  $assets_css['css-slick']       = 'plugins/slick/slick.min.css';
  $assets_css['css-slick-theme'] = 'plugins/slick/slick-theme.min.css';
  $assets_js['js-slick']         = 'plugins/slick/slick.min.js';
  

  /* Enqueue */
  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'underscore' );
  foreach ( $assets_css as $handle => $path ) {
    wp_enqueue_style( $handle, get_assets_path( $path ), [], ASSETS_VERSION );
  }
  foreach ( $assets_js as $handle => $path ) {
    wp_enqueue_script( $handle, get_assets_path( $path ), [], ASSETS_VERSION, true );
  }

  /* Conditions */
  wp_script_add_data( 'theme-html5', 'conditional', 'lt IE 9' );
  wp_script_add_data( 'theme-respond', 'conditional', 'lt IE 9' );

  /* Main assets */
 
  wp_enqueue_style( 'theme-dashicons', includes_url( "css/dashicons$suffix.css" ), [], ASSETS_VERSION );
  wp_enqueue_style( 'theme-style', get_theme_file_uri( 'dist/main.min.css' ), [], ASSETS_VERSION );
  wp_enqueue_style( 'theme-style-custom', get_theme_file_uri( 'assets/css/custom.css' ), [], ASSETS_VERSION );
  wp_enqueue_script( 'theme-js', get_theme_file_uri( 'dist/main.min.js' ), [], ASSETS_VERSION, true );
  wp_enqueue_script( 'swiper-js', 'https://unpkg.com/swiper@7/swiper-bundle.min.js', [], ASSETS_VERSION );
  wp_enqueue_style( 'swiper-css', 'https://unpkg.com/swiper@7/swiper-bundle.min.css', [], ASSETS_VERSION );
  

  wp_localize_script( 'theme-js',
    'ElementorStarter',
    [
      'ajaxUrl' => admin_url( 'admin-ajax.php' ),
    ]
  );
}

add_filter( 'excerpt_more', 'custom_excerpt_more' );
function custom_excerpt_more( $more ) {
  return ' ...';
}

add_action( 'wp_head', 'hook_head' );
function hook_head() {
  ?>
  <script type="text/javascript">var $ = jQuery.noConflict();</script>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
  <?php
}

add_action( 'wp_footer', 'custom_wp_footer', 99 );
function custom_wp_footer() {
}

/**
 * Register Dining and bars Post Type
 */

function register_dining_pt() {

	$labels = array(
		'name'                  => _x( 'Dining & Bars', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Dining & Bars', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Dining & Bars', 'text_domain' ),
		'name_admin_bar'        => __( 'Dining & Bars', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Dining & Bars', 'text_domain' ),
		'description'           => __( 'Dining & Bars Post Type', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_icon'             => 'dashicons-heart',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'dining-bars', $args );

}

add_action( 'init', 'register_dining_pt', 0 );

/**
 * Register Rooms Post Type
 */

function register_rooms_pt() {

	$labels = array(
		'name'                  => _x( 'Room Types', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Room Type', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Room Types', 'text_domain' ),
		'name_admin_bar'        => __( 'Room Types', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Room Types', 'text_domain' ),
		'description'           => __( 'Room Types Post Type', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_icon'             => 'dashicons-heart',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'room-type', $args );

}

add_action( 'init', 'register_rooms_pt', 0 );

/**
 * Register Rooms Post Type
 */

function register_reviews_pt() {

	$labels = array(
		'name'                  => _x( 'Reviews', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Review', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Reviews', 'text_domain' ),
		'name_admin_bar'        => __( 'Reviews', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Reviews', 'text_domain' ),
		'description'           => __( 'Reviews Post Type', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_icon'             => 'dashicons-heart',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'reviews', $args );

}

add_action( 'init', 'register_reviews_pt', 0 );

/**
 * Register Specials Post Type
 */

function register_specials_pt() {

	$labels = array(
		'name'                  => _x( 'Specials', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Special', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Specials', 'text_domain' ),
		'name_admin_bar'        => __( 'Specials', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Specials', 'text_domain' ),
		'description'           => __( 'Specials Post Type', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_icon'             => 'dashicons-tickets',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'special', $args );

}

add_action( 'init', 'register_specials_pt', 0 );

add_action( 'init', 'register_roomtypes_tax', 0 );
function register_roomtypes_tax() {
  $labels = array(
    'name'                       => _x( 'Types', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Type', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Types', 'text_domain' ),
    'all_items'                  => __( 'All Items', 'text_domain' ),
    'parent_item'                => __( 'Parent Item', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
    'new_item_name'              => __( 'New Item Name', 'text_domain' ),
    'add_new_item'               => __( 'Add New Item', 'text_domain' ),
    'edit_item'                  => __( 'Edit Item', 'text_domain' ),
    'update_item'                => __( 'Update Item', 'text_domain' ),
    'view_item'                  => __( 'View Item', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Items', 'text_domain' ),
    'search_items'               => __( 'Search Items', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'no_terms'                   => __( 'No items', 'text_domain' ),
    'items_list'                 => __( 'Items list', 'text_domain' ),
    'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
  );
  $args   = array(
    'labels'            => $labels,
    'hierarchical'      => true,
    'public'            => true,
    'show_ui'           => true,
    'show_admin_column' => true,
    'show_in_nav_menus' => true,
    'show_tagcloud'     => false,
  );
  register_taxonomy( 'types', array( 'room-type' ), $args );
}